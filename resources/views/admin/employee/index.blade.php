@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Pegawai</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Tabel</h4>
                    </div>
                    <div class="card-body p-0">
                        <div class="col-sm-12">
                            <a href="{{ route('employee.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Hak Akses</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @foreach ($user->roles as $role)
                                            {{ $role->description }}
                                        @endforeach                                    
                                    </td>
                                    <td>
                                        <form action="{{ route('employee.destroy', $user) }}" method="post">
                                            @csrf
                                            @method("DELETE")
                                            
                                            <a href="{{ route('employee.edit', $user) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                                            <button onclick="return confirm('Yakin ingin menghapus?')" type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            {{ $users->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@include('admin.templates.partials._notifications')