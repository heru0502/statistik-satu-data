@extends('admin.templates.default')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card p-2">
                    @role('admin')
                        <select class="form-control" id="office_id">
                            <option value="">- Pilih Dinas -</option>
                            @foreach($offices as $office)
                                <option value="{{ $office->id }}" {{ Route::input('id') == $office->id ? 'selected' : '' }}>{{ $office->name }}</option>
                            @endforeach
                        </select>
                    @endrole

                    @role('employee')
                        <h4>{{ $office->name }}</h4>
                    @endrole
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-calculator"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Indikator Target Masih Kosong Tahun Ini</h4>
                        </div>
                        <div class="card-body">
                            {{ $total_target_unfilled }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-calculator"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Indikator Realisasi Masih Kosong Tahun Ini</h4>
                        </div>
                        <div class="card-body">
                            {{ $total_realization_unfilled }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-calculator"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Rata-Rata Persentase Realisasi Tahun Ini</h4>
                        </div>
                        <div class="card-body">
                            {{ number_format($average_percentage_realization, 2) }} %
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-calculator"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Persentase Realisasi Terbesar Tahun Ini</h4>
                        </div>
                        <div class="card-body">
                            {{ number_format($max_percentage_realization, 2) }} %
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Rata-Rata Persentase Realisasi 7 Tahun Terakhir</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>
     </section>
@endsection

@push('scripts')
<script src="{{ asset('stisla/node_modules/chart.js/dist/Chart.min.js') }}"></script>

<script type="text/javascript">
    $('#office_id').change(function() {
        var id = $(this).val();
        window.location.href = "{{ route('dashboard.index') }}" + '/' +id;
    })

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: JSON.parse('{{ json_encode($years) }}'),
            datasets: [
            {
                label: 'Rata-rata persentase realisasi',
                data: JSON.parse('{{ json_encode($averages) }}'),
                borderWidth: 2,
                backgroundColor: 'rgba(63,82,227,.8)',
                borderWidth: 0,
                borderColor: 'transparent',
                pointBorderWidth: 0,
                pointRadius: 3.5,
                pointBackgroundColor: 'transparent',
                pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        // display: false,
                        drawBorder: false,
                        color: '#f2f2f2',
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1500,
                        callback: function(value, index, values) {
                            return value + ' %';
                        }
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false,
                        tickMarkLength: 15,
                    }
                }]
            },
        }
    });
</script>
@endpush