@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Perbarui Data Indikator</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('indicator.update', $indicator) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">

                            @role('admin')
                            <div class="form-group row">
                                <label for="office_id" class="col-sm-2 col-form-label">Dinas / SKPD</label>
                                <div class="col-sm-10">
                                    <select class="form-control"name="office_id" required="true">
                                        <option value=""> - </option>
                                        @foreach ($offices as $office)
                                            <option value="{{ $office->id }}" {{ $office->id === old('office_id', $indicator->office_id) ? 'selected' : '' }}>{{ $office->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('office_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            @endrole
                    
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name', $indicator->name) }}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="unit_id" class="col-sm-2 col-form-label">Satuan</label>
                                <div class="col-sm-10">
                                    <select class="form-control"name="unit_id" required="true">
                                        <option value=""> - </option>
                                        @foreach ($units as $unit)
                                            <option value="{{ $unit->id }}" {{ $unit->id === $indicator->unit_id ? 'selected' : '' }}>{{ $unit->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('unit_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('indicator.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection