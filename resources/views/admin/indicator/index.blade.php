@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Indikator</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Tabel</h4>
                    </div>
                    <div class="card-body p-0">
                        <div class="col-sm-12">
                            <a href="{{ route('indicator.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th>Name</th>
                                    <th>Satuan / Simbol</th>
                                    @role('admin')
                                        <th>Dinas / SKPD</th>
                                    @endrole
                                    <th>Action</th>
                                </tr>

                                @foreach($indicators as $indicator)
                                <tr>
                                    <td>{{ $indicator->name }}</td>
                                    <td>{{ $indicator->unit->name .' / '. $indicator->unit->symbol }}</td>
                                    @role('admin')
                                        <td>{{ $indicator->office->name }}</td>
                                    @endrole
                                    <td>
                                        <form action="{{ route('indicator.destroy', $indicator) }}" method="post">
                                            @csrf
                                            @method("DELETE")
                                            
                                            <a href="{{ route('indicator.edit', $indicator) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                                            <button onclick="return confirm('Yakin ingin menghapus?')" type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            {{ $indicators->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@include('admin.templates.partials._notifications')