
@foreach ($indicators as $indicator)
    <tr>
        <td>
            {!! $indicator->indicator_id === null ? "<b>$indicator->name</b>" : $nestedSpace . $indicator->name !!}
            {{ ' ' . $indicator->children->count() }}
        </td>
            
        <td>
            <form action="{{ route('indicator.destroy', $indicator) }}" method="post">
                @csrf
                @method("DELETE")
                
                <a href="{{ route('indicator.edit', $indicator) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                <button onclick="return confirm('Yakin ingin menghapus?')" type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
            </form>
        </td>
    </tr>

    @php
        // if ($indicator->children->count() > 0) {
        //     $nestedSpace .= '<i class="fas fa-circle"></i> ';
        //     $depthLevel++;
        // } 
        // else if ($indicator->children->count() === 0 && $indicator->indicator_id !== null) {
        //     $nestedSpace = '<i class="fas fa-circle"></i> ';
        // } 
        // else {
        //     $nestedSpace = '';   
        // }

        
    @endphp

    @include('admin.indicator.recursive', ['indicators' => $indicator->children, 'nestedSpace' => $nestedSpace, 'depthLevel' => $depthLevel])

@endforeach