
@foreach ($indicators as $indicator)

    <tr>
        <td>
            @if ($indicator->indicator_id === null)
                <ul>
                    <li>{{ $indicator->name }}</li>
                    @if ($indicator->children->count() > 0)
                        @include('admin.indicator.recursive2', ['indicators' => $indicator->children])
                    @endif
                </ul>
            @endif
        </td>
        <td> </td>
    </tr>


    {{-- <ul>
        <li>{{ $indicator->name }}</li>
        @if ($indicator->children->count() > 0)
            @include('admin.indicator.recursive2', ['indicators' => $indicator->children])
        @endif
    </ul> --}}
   

@endforeach