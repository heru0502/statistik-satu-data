@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Dinas / OPD</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Tabel</h4>
                    </div>
                    <div class="card-body p-0">
                        <div class="col-sm-12">
                            <a href="{{ route('office.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($offices as $office)
                                <tr>
                                    <td>{{ $office->name }}</td>
                                    <td>
                                        <form action="{{ route('office.destroy', $office) }}" method="post">
                                            @csrf
                                            @method("DELETE")
                                            
                                            <a href="{{ route('office.edit', $office) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                                            <button onclick="return confirm('Yakin ingin menghapus?')" type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            {{ $offices->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@include('admin.templates.partials._notifications')