@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Laporan</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tabel</h4>
                    </div>
                    <div class="card-body p-3">
                        <form action="{{ route('report.report') }}" method="post">
                            @csrf

                            <div class="row">
                                @role('admin')
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Dinas / SKPD</label>
                                            <select class="form-control"name="office_id" required="true">
                                                <option value=""> - </option>
                                                @foreach ($offices as $office)
                                                    <option value="{{ $office->id }}" 
                                                        @if($reports !== null)
                                                            {{ $office->id == $post['office_id'] ? 'selected' : '' }}
                                                        @endif
                                                    >
                                                        {{ $office->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('year_start')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                @endrole
                                
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Dari Tahun</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-calendar"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="year_start" value="{{ $reports !== null ? $post['year_start'] : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Sampai Tahun</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-calendar"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="year_end" value="{{ $reports !== null ? $post['year_end'] : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <a href="{{ route('report.index') }}" class="btn btn-block btn-warning"><i class="fa fa-eraser"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <button onclick="window.print()" class="btn btn-warning"><i class="fa fa-print"></i> Cetak</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <div id="printcontent">
                                @if($reports !== null)

                                    <span id="title" class="text-center">Laporan Data {{ $office->name }} Periode {{ $post['year_start'] }} - {{ $post['year_end'] }}</span>
                                    <br>

                                    <table class="table table-bordered table-md" style="margin-top: 0px; padding-top: 0px">
                                        {{-- Header --}}
                                        <tr>
                                            <th rowspan="3" class="align-middle text-center">Indikator Kinerja Utama</th>
                                            <th colspan="{{ count($years) }}" class="text-center">Target</th>
                                            <th colspan="{{ count($years) * 2 }}" class="text-center">Realisasi</th>
                                            <th rowspan="3" class="align-middle text-center">Satuan</th>
                                        </tr>

                                        {{-- 
                                        - Dynamic header year   
                                        --}}
                                        <tr>
                                            @for($i=0; $i<2; $i++)
                                                @foreach($years as $year)
                                                    <th colspan="{{ $i > 0 ? '2' : '' }}" class="text-center">{{ $year }}</th>
                                                @endforeach
                                            @endfor
                                        </tr>

                                        {{--
                                        - Dynamic header Angka based year
                                        --}}
                                        <tr>
                                            @for($i=0; $i<2; $i++)
                                                @if($i < 1)
                                                    @foreach($years as $year)
                                                        <th class="text-center">Angka</th>
                                                    @endforeach
                                                @else
                                                    @foreach($years as $year)
                                                        <th class="text-center">Angka</th>
                                                        <th class="text-center">%</th>
                                                    @endforeach
                                                @endif
                                            @endfor
                                        </tr>
                                    
                                        {{-- Body --}}
                                        @foreach($reports as $key => $report)
                                        <tr>
                                            <td>{{ $report['indicator_name'] }}</td>
                                            
                                            @for($i = 0; $i < 2; $i++)

                                                {{-- Amount Target Column --}}
                                                @if($i < 1)
                                                    {{--
                                                    - Print column which match with selected year 
                                                    --}}
                                                    @foreach($years as $year)
                                                        @php($year_found = false)
                                                        @foreach($report['amount_target'] as $key_year => $amount_target)
                                                            @if($key_year === $year)
                                                                <td>{{ $amount_target }}</td>
                                                                @php($year_found = true)
                                                            @endif
                                                        @endforeach

                                                        @if(!$year_found)
                                                            <td>-</td>
                                                        @endif
                                                    @endforeach
                                                    
                                                {{-- Amount Realization Column --}}
                                                @else
                                                    {{--
                                                    - Print column which match with selected year 
                                                    --}}
                                                    @foreach($years as $year)
                                                        @php($year_found = false)
                                                        @foreach($report['amount_realization'] as $key_year => $amount_realization)
                                                            @if($key_year === $year)
                                                                <td>{{ $report['amount_realization'][$year] !== null ? $report['amount_realization'][$year] : '-' }}</td>
                                                                <td>{{ number_format($report['percentage_realization'][$year], 2) !== null ? number_format($report['percentage_realization'][$year], 2) : '-' }}</td>
                                                                @php($year_found = true)
                                                            @endif
                                                        @endforeach

                                                        @if(!$year_found)
                                                            <td>-</td>
                                                            <td>-</td>
                                                        @endif
                                                    @endforeach

                                                @endif
                                            @endfor
                                            
                                            <td>{{ $report['unit_name'] }}</td>
                                        </tr>
                                        @endforeach
                                    
                                    </table>
                                @else
                                    <h1 class="text-center">Tidak ada data . . .</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('styles')    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
    <style>
        @media print
        {
            body * { 
                visibility: hidden;                 
            }
            #printcontent * { visibility: visible; }
            #printcontent { 
                position: absolute;  
                top: 0px;
            }
            @page { size: auto; }
        }
        #title {
            font-weight: bold;
            font-size: 30px;
            margin: 0 auto;
            display: table;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                autoclose: true,
                format: "yyyy",
                minViewMode: "years"
            });
        });        
    </script> 
@endpush