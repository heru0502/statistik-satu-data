@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Perbarui Data Satuan</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('unit.update', $unit) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">
                    
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name', $unit->name) }}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="symbol" class="col-sm-2 col-form-label">Simbol</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control @error('symbol') is-invalid @enderror" id="symbol" name="symbol" value="{{ old('symbol', $unit->symbol) }}" required="true">
                                    @error('symbol')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="symbol_position" class="col-sm-2 col-form-label">Letak Simbol</label>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="symbol_position_1" name="symbol_position" class="custom-control-input" value="left" required="true" {{ old('symbol_position', $unit->symbol_position) === 'left' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="symbol_position_1">Kiri</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="symbol_position_2" name="symbol_position" class="custom-control-input" value="right" required="true" {{ old('symbol_position', $unit->symbol_position) === 'right' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="symbol_position_2">Kanan</label>
                                    </div>
                                    @error('symbol_position')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="number_format" class="col-sm-2 col-form-label">Format Angka</label>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="number_format_1" name="number_format" class="custom-control-input" value="integer" required="true" {{ old('number_format', $unit->number_format) === 'integer' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="number_format_1">Bilangan Bulat</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="number_format_2" name="number_format" class="custom-control-input" value="decimal" required="true" {{ old('number_format', $unit->number_format) === 'decimal' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="number_format_2">Desimal</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="number_format_2" name="number_format" class="custom-control-input" value="currency" required="true" {{ old('number_format', $unit->number_format) === 'currency' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="number_format_2">Mata Uang</label>
                                    </div>
                                    @error('number_format')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('unit.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection