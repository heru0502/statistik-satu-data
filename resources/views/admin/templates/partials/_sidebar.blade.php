<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Statistik Satu Data</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">SSD</a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ request()->is('admin/dashboard*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('dashboard.index') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li> 
            <li class="nav-item dropdown {{ request()->is('admin/office*','admin/employee*','admin/unit*','admin/indicator*') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-cogs"></i><span>Pengaturan</span></a>
                <ul class="dropdown-menu">

                    @role('admin')
                        <li class="{{ request()->is('admin/office*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('office.index') }}">Dinas / OPD</a></li>
                        <li class="{{ request()->is('admin/employee*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('employee.index') }}">Pegawai</a></li>
                    @endrole
                    
                    <li class="{{ request()->is('admin/unit*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('unit.index') }}">Satuan</a></li>
                    <li class="{{ request()->is('admin/indicator*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('indicator.index') }}">Indikator</a></li>
                </ul>
            </li>   
            <li class="{{ request()->is('admin/performance-indicator*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('performance-indicator.index') }}"><i class="fas fa-inbox"></i> <span>Indikator Kinerja</span></a></li> 
            <li class="{{ request()->is('admin/report*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('report.index') }}"><i class="fa fa-file"></i> <span>Laporan</span></a></li> 
        </ul>
    </aside>
</div>