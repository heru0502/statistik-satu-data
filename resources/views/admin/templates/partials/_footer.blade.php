<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2019 <div class="bullet"></div> Powered By <a href="https://kominfo.banjarkab.go.id/" target="_blank">Diskominfo Statistik dan Persandian Kab. Banjar</a>
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>