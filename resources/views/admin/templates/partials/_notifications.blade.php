@push('styles')
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/izitoast/dist/css/iziToast.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('stisla/node_modules/izitoast/dist/js/iziToast.min.js') }}"></script>

    @if (session('success'))
        <script>
            let message = '';        
            if ("{{ session('success') }}" === 'store') {
                message = 'Data telah tersimpan.' 
            } 
            else if ("{{ session('success') }}" === 'update') {
                message = 'Data telah diperbarui.' 
            } 
            else if ("{{ session('success') }}" === 'destroy') {
                message = 'Data telah terhapus.' 
            }

            iziToast.success({
                title: 'Berhasil!',
                message: message,
                position: 'topRight'
            });        
        </script>

    @elseif (session('warning'))
        <script>        
            iziToast.warning({
                title: 'Hey',
                message: 'What would you like to add?',
                position: 'topRight'
            });        
        </script>    
    @endif
@endpush