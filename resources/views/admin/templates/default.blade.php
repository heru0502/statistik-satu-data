<!DOCTYPE html>
<html lang="en">
@include('admin.templates.partials._head')

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      @include('admin.templates.partials._navbar')

      @include('admin.templates.partials._sidebar')

      <!-- Main Content -->
      <div class="main-content">
        @yield('content')
      </div>
      
      @include('admin.templates.partials._footer')
    </div>
  </div>

  @include('admin.templates.partials._scripts')
</body>
</html>
