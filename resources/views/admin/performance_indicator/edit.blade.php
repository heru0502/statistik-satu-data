@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Perbarui Data Indikator Kinerja</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('performance-indicator.update', $office->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="office_id" class="col-sm-2 col-form-label">Dinas / OPD</label>
                                <div class="col-sm-10">
                                    {{ $office->name }}
                                    <input type="hidden" name="office_id" value="{{ $office->id }}">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="year" class="col-sm-2 col-form-label">Tahun</label>
                                <div class="col-sm-2">
                                    {{ $year }}    
                                    <input type="hidden" name="year" value="{{ $year }}">    
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Indikator Kinerja Utama</label>
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Indikator</th>
                                            <th>Target</th>
                                            <th>Ralisasi</th>
                                            <th>Satuan</th>
                                            <th>%</th>
                                        </tr>
                    
                                        @foreach($filled_performance_indicators as $key => $filled_performance_indicator)
                                        
                                        <input type="hidden" name="id_indicators[]" value="{{ $filled_performance_indicator['indicator_id'] }}">
                                        <input type="hidden" name="id_performance_indicators[]" value="{{ $filled_performance_indicator['id'] }}">

                                        <tr>
                                            <td>{{ $filled_performance_indicator['indicator_name'] }}</td>
                                            <td><input type="text" class="form-control" name="amount_target[]" value="{{ $filled_performance_indicator['amount_target'] }}"></td>
                                            <td><input type="text" class="form-control" name="amount_realization[]" value="{{ $filled_performance_indicator['amount_realization'] }}"></td>
                                            <td>{{ $filled_performance_indicator['unit_name'] }}</td>
                                            <td>{{ number_format($filled_performance_indicator['percentage_realization'], 2) }}</td>
                                            
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>                   
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('performance-indicator.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
