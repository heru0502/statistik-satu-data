@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Tambah Data Indikator Kinerja</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('performance-indicator.store') }}" method="post">
                        @csrf
                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">

                            @role('admin')
                                <div id="app">
                                    <performance-indicator-create></performance-indicator-create>
                                </div>      
                            @endrole      
                            
                            @role('employee')
                                @include('admin.performance_indicator.partials._indicator_table')
                            @endrole
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('performance-indicator.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endpush