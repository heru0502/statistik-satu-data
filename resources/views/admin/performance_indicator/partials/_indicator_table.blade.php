<div class="form-group row">
    <label for="office_id" class="col-sm-2 col-form-label">Dinas / OPD</label>
    <div class="col-sm-10">
        {{ $office->name }}
    </div>
</div>

<div class="form-group row">
    <label for="year" class="col-sm-2 col-form-label">Tahun</label>
    <div class="col-sm-2">
        <select name="year" class="form-control" required="true">
            <option value=""> - </option>
            @foreach($years as $year)
                <option {{ !$year['available'] ? 'disabled' : '' }}>{{ $year['name'] }}</option>
            @endforeach
        </select>           
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Indikator Kinerja Utama</label>
    <div class="col-sm-10">
        <table class="table table-bordered">
            <tr>
                <th>Indikator</th>
                <th>Target</th>
                <th>Ralisasi</th>
                <th>Satuan</th>
            </tr>

            @foreach($indicators as $indicator)
                <tr>
                    <input type="hidden" name="id_indicators[]" value="{{ $indicator->id }}">
                    <td>{{ $indicator->name }}</td>
                    <td><input type="text" class="form-control" name="amount_target[]"></td>
                    <td><input type="text" class="form-control" name="amount_realization[]"></td>
                    <td>{{ $indicator->unit->name }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div> 