@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Indikator Kinerja</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Tabel</h4>
                    </div>
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-sm-9">
                                <a id="add" href="{{ route('performance-indicator.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th>Kantor</th>
                                    <th>Tahun</th>
                                    <th>Action</th>
                                </tr>

                                @foreach($performance_indicators as $performance_indicator)
                                <tr>
                                    <td>{{ $performance_indicator->office->name }}</td>
                                    <td>{{ $performance_indicator->year }}</td>
                                    <td>
                                        <form action="{{ route('performance-indicator.destroy', $performance_indicator) }}" method="post">
                                            @csrf
                                            @method("DELETE")
                                            
                                            <a href="{{ route('performance-indicator.edit', [$performance_indicator->office_id, $performance_indicator->year]) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Perbarui</a>
                                            <button onclick="return confirm('Yakin ingin menghapus?')" type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                            {{ $performance_indicators->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@include('admin.templates.partials._notifications')
