require('./bootstrap');

window.Vue = require('vue');

Vue.component('performance-indicator-create', require('./components/performance_indicator/CreateComponent.vue').default);


const app = new Vue({
    el: '#app',
});
