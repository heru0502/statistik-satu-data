<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Office;
use App\Models\Indicator;
use App\Models\PerformanceIndicator;

class PerformanceIndicatorController extends Controller
{
    public function offices()
    {
        return Office::all();
    }

    public function indicators($id)
    {
        return Indicator::where('office_id', $id)->with('unit')->get();
    }

    public function years($id)
    {
        $performance_indicators = PerformanceIndicator::where('office_id', $id)->groupBy('year')->get();
        foreach (range(2019, 2000) as $key => $year) {
            $years[$key]['name'] = $year;
            
            if ($performance_indicators->count() > 0) {
                $found = false;
                foreach ($performance_indicators as $performance_indicator) {
                    if ($performance_indicator->year === $year) {
                        $found = true;
                        $years[$key]['available'] = false;
                    } 
                }

                if (!$found) {
                    $years[$key]['available'] = true;
                }
            }
            else {
                $years[$key]['available'] = true;
            }
        } 
        return $years;
    }
    
}
