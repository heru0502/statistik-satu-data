<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\User;    
use App\Models\Role;
use App\Models\Office;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->orderBy('id', 'desc')->paginate(10);
        return view('admin.employee.index', compact('users', $users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $offices = Office::orderBy('name')->get();
        return view('admin.employee.create', [
            'roles' => $roles,
            'offices' => $offices
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
        ]);

        DB::transaction(function() use ($request) {
            $employee = new User;
            $employee->name = $request->name;
            $employee->email = $request->email;
            $employee->password = bcrypt($request->password);
            $employee->save();

            $user = User::find($employee->id);
            $user->roles()->attach($request->role);

            if ($request->office) {
                $user->offices()->attach($request->office);
            }
        });
        
        return redirect()->route('employee.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('roles')->find($id);
        $offices = Office::orderBy('name')->get();
        $roles = Role::all();
        return view('admin.employee.edit', compact('user','offices','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',
            'office' => 'required'
        ]);

        DB::transaction(function() use ($request, $id) {
            $employee = User::find($id);
            $employee->name = $request->name;
            $employee->email = $request->email;
            
            if ($request->filled('password')) {
                $employee->password = bcrypt($request->password);
            }

            $employee->update();

            $user = User::find($id);
            $user->roles()->detach();
            $user->roles()->attach($request->role);
            $user->offices()->detach();
            $user->offices()->attach($request->office);
        });
        
        return redirect()->route('employee.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = User::find($id);
        $employee->delete();
        
        return back()->with('success', 'destroy');
    }
}
