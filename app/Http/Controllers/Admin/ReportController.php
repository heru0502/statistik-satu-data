<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Office;
use App\Models\PerformanceIndicator;

class ReportController extends Controller
{
    public function index()
    {
        $offices = Office::all();
        $reports = null;
        return view('admin.report.index', compact('offices','reports'));
    }

    public function report(Request $request)
    {
        $request->validate([
            // 'office_id' => 'required',
            'year_start' => 'required',
            'year_end' => 'required'
        ]);

        $years = range($request->year_start, $request->year_end);
        $post = $request->all();

        $office_id = Auth::user()->hasRole('admin') ? $request->office_id : session('office_id');
        
        $offices = Office::all();
        $office = Office::find($office_id);
        $performance_indicators = PerformanceIndicator::where('office_id', $office_id)
                    ->whereBetween('year', [$request->year_start, $request->year_end])
                    ->orderBy('indicator_id', 'asc')
                    ->orderBy('year', 'asc')
                    ->get();

        $reports = null; 
        $last_indicator_id = null;
        $last_key = 0;
        if ($performance_indicators !== null) {
            foreach($performance_indicators as $key => $performance_indicator) {

                /** New row if indicator_id not same previously */
                if ($last_indicator_id !== $performance_indicator->indicator_id) {
                    $reports[$key]['indicator_id'] = $performance_indicator->indicator_id;
                    $reports[$key]['indicator_name'] = $performance_indicator->indicator->name;
                    $reports[$key]['unit_name'] = $performance_indicator->indicator->unit->name;
                    $reports[$key]['amount_target'][$performance_indicator->year] = $performance_indicator->amount_target;
                    $reports[$key]['amount_realization'][$performance_indicator->year] = $performance_indicator->amount_realization;
                    $reports[$key]['percentage_realization'][$performance_indicator->year] = $performance_indicator->percentage_realization;
                    $last_key = $key;            
                }
                /** Continue previous row if indicator_id same previously */
                else {
                    $reports[$last_key]['amount_target'][$performance_indicator->year] = $performance_indicator->amount_target;
                    $reports[$last_key]['amount_realization'][$performance_indicator->year] = $performance_indicator->amount_realization;
                    $reports[$last_key]['percentage_realization'][$performance_indicator->year] = $performance_indicator->percentage_realization;
                }

                $last_indicator_id = $performance_indicator->indicator_id;                
            }
        }

        /** Sorting by Indicator Name */
        if ($reports !== null) {
            $keys = array_column($reports, 'indicator_name');
            array_multisort($keys, SORT_ASC, $reports);
        }

        return view('admin.report.index', compact('reports','offices','office','years','post'));
    }
}
