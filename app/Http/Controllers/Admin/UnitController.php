<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Unit;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::where(function ($query) {
            if (Auth::user()->hasRole('employee')) {
                $query->where('office_id', session('office_id'))
                    ->orWhere('office_id', null);
            }
        })
        ->orderBy('id', 'desc')->paginate(10);
        return view('admin.unit.index', compact('units', $units));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'symbol' => 'required',
            'symbol_position' => 'required'
        ]);

        $unit = new Unit;
        $unit->name = $request->name;
        $unit->symbol = $request->symbol;
        $unit->symbol_position = $request->symbol_position;
        $unit->number_format = $request->number_format;
        $unit->created_by = Auth::id();

        if (Auth::user()->hasRole('employee')) {
            $unit->office_id = session('office_id');
        }

        $unit->save();

        return redirect()->route('unit.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id);
        return view('admin.unit.edit', compact('unit', $unit));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'symbol' => 'required',
            'symbol_position' => 'required'
        ]);

        $unit = Unit::find($id);
        $unit->name = $request->name;
        $unit->symbol = $request->symbol;
        $unit->symbol_position = $request->symbol_position;
        $unit->number_format = $request->number_format;
        $unit->updated_by = Auth::id();

        if (Auth::user()->hasRole('employee')) {
            $unit->office_id = session('office_id');
        }

        $unit->update();

        return redirect()->route('unit.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::find($id);
        $unit->delete();

        return back()->with('success', 'destroy');
    }
}
