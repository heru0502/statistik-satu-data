<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Indicator;
use App\Models\Unit;
use App\Models\Office;
use App\Models\User;

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicators = Indicator::whereHas('user.offices', function ($q) {
            if (Auth::user()->hasRole('employee')) {
                $q->where('id', session('office_id'));
            }
        })
        ->orderBy('id', 'desc')->paginate(10);
        return view('admin.indicator.index', compact('indicators', $indicators));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offices = Office::all();
        $units = Unit::orderBy('name', 'asc')->get();
        return view('admin.indicator.create', compact('offices','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'office_id' => 'required',
            'name' => 'required',
            'unit_id' => 'required'
        ]);

        $indicator = new Indicator;
        $indicator->name = $request->name;
        $indicator->unit_id = $request->unit_id;
        $indicator->created_by = Auth::id();

        if (Auth::user()->hasRole('admin')) {
            $indicator->office_id = $request->office_id;
        }else {
            $indicator->office_id = session('office_id');
        }

        // if ($request->filled('indicator_id'))
        //     $indicator->indicator_id = $request->indicator_id;

        $indicator->save();
        
        return redirect()->route('indicator.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offices = Office::all();
        $indicator = Indicator::find($id);
        $units = Unit::orderBy('name', 'asc')->get();
        return view('admin.indicator.edit', compact('indicator','units','offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'unit_id' => 'required'
        ]);

        $indicator = Indicator::find($id);
        $indicator->name = $request->name;
        $indicator->unit_id = $request->unit_id;
        $indicator->updated_by = Auth::id();

        if (Auth::user()->hasRole('admin')) {
            $indicator->office_id = $request->office_id;
        }else {
            $indicator->office_id = session('office_id');
        }
        
        $indicator->update();
        
        return redirect()->route('indicator.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indicator = Indicator::find($id);
        $indicator->delete();
        
        return back()->with('success', 'destroy');
    }
}
