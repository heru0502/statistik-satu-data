<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\PerformanceIndicator;
use App\Models\Indicator;
use App\Models\Office;

class PerformanceIndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $performance_indicators = PerformanceIndicator::whereHas('user.offices', function ($q) {
            if (Auth::user()->hasRole('employee')) {
                $q->where('id', session('office_id'));
            }
        })
        ->orderBy('id', 'desc')->groupBy('year')->paginate(10);
        return view('admin.performance_indicator.index', compact('performance_indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->hasRole('employee')) {
            $office = Auth::user()->offices->first();
            $years = $this->years(session('office_id'));
            $indicators = Indicator::where('office_id', Auth::user()->offices->first()->id)->with('unit')->get();
            return view('admin.performance_indicator.create', compact('office','years','indicators'));
        }
        else {
            return view('admin.performance_indicator.create');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'year' => 'required',
        ]);

        DB::transaction(function () use ($request) {
            
            for ($i = 0; $i < count($request->id_indicators); $i++) 
            {
                if ($request->amount_target[$i] !== null) {
                    $performance_indicator = new PerformanceIndicator;
                    $performance_indicator->office_id = session('office_id');
                    $performance_indicator->indicator_id = $request->id_indicators[$i];
                    $performance_indicator->year = $request->year;
                    $performance_indicator->amount_target = $request->amount_target[$i];

                    if ($request->amount_realization[$i] !== null) {                        
                        $performance_indicator->amount_realization = $request->amount_realization[$i];
                        $performance_indicator->percentage_realization = ($request->amount_realization[$i] / $request->amount_target[$i]) * 100;
                    }

                    $performance_indicator->created_by = Auth::id();
                    $performance_indicator->save();
                }
            }

        });

        return redirect()->route('performance-indicator.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($office_id, $year)
    {
        $indicators = Indicator::where('office_id', $office_id)->get();
        $office = Office::find($office_id);
        $performance_indicators = PerformanceIndicator::where([
            ['office_id', '=', $office_id],
            ['year', '=', $year]
        ])->get();

        foreach($indicators as $key => $indicator) {
            $found = false;
            foreach($performance_indicators as $performance_indicator) {
                if ($indicator->id === $performance_indicator->indicator_id) {
                    $found = true;
                    $filled_performance_indicators[$key]['id'] = $performance_indicator->id;
                    $filled_performance_indicators[$key]['indicator_id'] = $indicator->id;
                    $filled_performance_indicators[$key]['indicator_name'] = $indicator->name;
                    $filled_performance_indicators[$key]['amount_target'] = $performance_indicator->amount_target;
                    $filled_performance_indicators[$key]['amount_realization'] = $performance_indicator->amount_realization;
                    $filled_performance_indicators[$key]['percentage_realization'] = $performance_indicator->percentage_realization;
                    $filled_performance_indicators[$key]['unit_name'] = $indicator->unit->name;
                }
            }

            if (!$found) {
                $filled_performance_indicators[$key]['id'] = null;
                $filled_performance_indicators[$key]['indicator_id'] = $indicator->id;
                $filled_performance_indicators[$key]['indicator_name'] = $indicator->name;
                $filled_performance_indicators[$key]['amount_target'] = null;
                $filled_performance_indicators[$key]['amount_realization'] = null;
                $filled_performance_indicators[$key]['percentage_realization'] = null;
                $filled_performance_indicators[$key]['unit_name'] = $indicator->unit->name;
            }
        }
        // dd($filled_performance_indicators);
        return view('admin.performance_indicator.edit', compact('office','year','performance_indicators','indicators','filled_performance_indicators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request) 
        {
            for ($i = 0; $i < count($request->id_performance_indicators); $i++) 
            {
                if ($request->id_performance_indicators[$i] !== null) {
                    $performance_indicator = PerformanceIndicator::find($request->id_performance_indicators[$i]);
                    $performance_indicator->delete();
                }
            }

            for ($i = 0; $i < count($request->id_indicators); $i++) 
            {
                if ($request->amount_target[$i] !== null) {
                    $performance_indicator = new PerformanceIndicator;
                    $performance_indicator->office_id = $request->office_id;
                    $performance_indicator->indicator_id = $request->id_indicators[$i];
                    $performance_indicator->year = $request->year;
                    $performance_indicator->amount_target = $request->amount_target[$i];

                    if ($request->amount_realization[$i] !== null) {                        
                        $performance_indicator->amount_realization = $request->amount_realization[$i];
                        $performance_indicator->percentage_realization = ($request->amount_realization[$i] / $request->amount_target[$i]) * 100;
                    }

                    $performance_indicator->created_by = Auth::id();
                    $performance_indicator->save();
                }
            }
        });

        return redirect()->route('performance-indicator.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $performance_indicator = PerformanceIndicator::find($id);
        $performance_indicator->delete();
        
        return back()->with('success', 'destroy');
    }

    private function years($id)
    {
        $performance_indicators = PerformanceIndicator::where('office_id', $id)->groupBy('year')->get();
        foreach (range(2019, 2000) as $key => $year) {
            $years[$key]['name'] = $year;
            
            if ($performance_indicators->count() > 0) {
                $found = false;
                foreach ($performance_indicators as $performance_indicator) {
                    if ($performance_indicator->year === $year) {
                        $found = true;
                        $years[$key]['available'] = false;
                    } 
                }

                if (!$found) {
                    $years[$key]['available'] = true;
                }
            }
            else {
                $years[$key]['available'] = true;
            }
        } 
        return $years;
    }
}
