<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Office;
use App\Models\PerformanceIndicator;
use App\Models\Indicator;

class DashboardController extends Controller
{
    public function index($id = null)
    {
        $current_year = date('Y');
        $offices = Office::all();
        $office = Office::find(session('office_id')); 
        $office_id = Auth::user()->hasRole('admin') ? $id : session('office_id');
        $total_indicator = Indicator::where('office_id', $office_id)->get();

        /**
         * Row which counting total
         */
        $total_target = PerformanceIndicator::where([
            ['office_id', '=', $office_id],
            ['year', '=', $current_year],
        ])->get();
        $total_target_unfilled = $total_indicator->count() - $total_target->count();

        $total_realization = PerformanceIndicator::where([
            ['office_id', '=', $office_id],
            ['year', '=', $current_year],
            ['amount_realization', '!=', null]
        ])->get();
        $total_realization_unfilled = $total_indicator->count() - $total_realization->count();
        
        $average_percentage_realization = PerformanceIndicator::where([
            ['office_id', '=', $office_id],
            ['year', '=', $current_year],
            ['percentage_realization', '!=', null]
        ])
        ->avg('percentage_realization');

        $max_percentage_realization = PerformanceIndicator::where([
            ['office_id', '=', $office_id],
            ['year', '=', $current_year],
            ['percentage_realization', '!=', null]
        ])
        ->max('percentage_realization');

        /**
         * Chart which is average percentage realization last 7 year
         */
        $avg_percentages = PerformanceIndicator::selectRaw('AVG(percentage_realization) AS average, year')
            ->where('office_id', $office_id)
            ->whereBetween('year', [$current_year-6, $current_year])
            ->groupBy('year')
            ->orderBy('year', 'asc')
            ->get();

        foreach (range($current_year-6, $current_year) as $year) {
            $years[] = $year;
            $year_found = false;
            foreach ($avg_percentages as $avg_percentage) {
                if ($avg_percentage->year === $year && $avg_percentage->average !== null) {
                    $averages[] = (float) number_format($avg_percentage->average, 2);
                    $year_found = true;
                }
            }

            if (!$year_found) {
                $averages[] = 0;
            }
        }
        
        return view('admin.dashboard.index', compact(
            'offices',
            'office',
            'total_target_unfilled',
            'total_realization_unfilled',
            'average_percentage_realization',
            'max_percentage_realization',
            'years',
            'averages'
        ));
    }
}
