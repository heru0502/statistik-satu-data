<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    public function children()
    {
        return $this->hasMany('App\Models\Indicator', 'indicator_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }

    public function performance_indicator()
    {
        return $this->hasOne('App\Models\PerformanceIndicator', 'indicator_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
