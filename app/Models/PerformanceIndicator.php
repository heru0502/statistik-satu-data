<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceIndicator extends Model
{
    public function indicator()
    {
        return $this->belongsTo('App\Models\Indicator', 'indicator_id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Office', 'office_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
