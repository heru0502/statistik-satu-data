<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Kominfo',
                'email' => 'kominfo@banjarkab.go.id',
                'password' => bcrypt('rahasia')
            ],
            [
                'name' => 'Diana',
                'email' => 'diana@banjarkab.go.id',
                'password' => bcrypt('rahasia')
            ],
            [
                'name' => 'Heru Firmansyah',
                'email' => 'heru@banjarkab.go.id',
                'password' => bcrypt('rahasia')
            ],
            [
                'name' => 'Arif',
                'email' => 'arif@banjarkab.go.id',
                'password' => bcrypt('rahasia')
            ]
        ]);
    }
}
