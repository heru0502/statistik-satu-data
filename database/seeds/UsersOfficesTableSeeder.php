<?php

use Illuminate\Database\Seeder;

class UsersOfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_offices')->insert([
            [
                'user_id' => 1,
                'office_id' => 1
            ],
            [
                'user_id' => 2,
                'office_id' => 1
            ],
            [
                'user_id' => 3,
                'office_id' => 1
            ],
            [
                'user_id' => 4,
                'office_id' => 13
            ]
        ]);
    }
}
