<?php

use Illuminate\Database\Seeder;

class OfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offices')->insert([
            [
                'name' => 'DINAS KOMINFO STATISTIK DAN PERSANDIAN',
            ],
            [
                'name' => 'INSPEKTORAT'
            ],
            [
                'name' => 'BADAN KEPEGAWAIAN DAERAH DAN PENGEMBANGAN SUMBER DAYA MANUSIA'
            ],
            [
                'name' => 'BADAN PENANGGULANGAN BENCANA DAERAH'
            ],
            [
                'name' => 'BADAN KESATUAN BANGSA DAN POLITIK'
            ],
            [
                'name' => 'BADAN PENDAPATAN DAERAH'
            ],
            [
                'name' => 'BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH'
            ],
            [
                'name' => 'RUMAH SAKIT UMUM DAERAH RATU ZALECHA'
            ],
            [
                'name' => 'DINAS PENDIDIKAN'
            ],
            [
                'name' => 'DINAS PERUMAHAN DAN PEMUKIMAN'
            ],
            [
                'name' => 'DINAS PERTANAHAN'
            ],
            [
                'name' => 'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG'
            ],
            [
                'name' => 'DINAS PETERNAKAN DAN PERKEBUNAN'
            ],
            [
                'name' => 'DINAS TANAMAN PANGAN DAN HORTIKULTURA'
            ],
            [
                'name' => 'DINAS KEBUDAYAAN DAN PARIWISATA'
            ],
            [
                'name' => 'DINAS PEMUDA DAN OLAHRAGA'
            ],
            [
                'name' => 'DINAS PERHUBUNGAN'
            ],
            [
                'name' => 'DINAS KESEHATAN'
            ],
            [
                'name' => 'DINAS SOSIAL'
            ],
            [
                'name' => 'DINAS PEMBERDAYAAN MASYARAKAT DAN DESA'
            ],
            [
                'name' => 'DINAS TENAGA KERJA DAN TRANSMIGRASI'
            ],
            [
                'name' => 'DINAS PENGENDALIAN PENDUDUK, KELUARGA BERENCANA, PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK'
            ],
            [
                'name' => 'DINAS KETAHANAN PANGAN'
            ],
            [
                'name' => 'DINAS LINGKUNGAN HIDUP'
            ],
            [
                'name' => 'DINAS PERINDUSTRIAN DAN PERDAGANGAN'
            ],
            [
                'name' => 'DINAS KOPERASI DAN USAHA MIKRO'
            ],
            [
                'name' => 'DINAS PERPUSTAKAAN DAN KEARSIPAN'
            ],
            [
                'name' => 'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL'
            ],
            [
                'name' => 'DINAS PENANAMAN MODAL DAN PTSP'
            ],
            [
                'name' => 'DINAS PERIKANAN'
            ],
            [
                'name' => 'SATUAN POLISI PAMONG PRAJA'
            ],
            [
                'name' => 'PDAM INTAN BANJAR'
            ],
            [
                'name' => 'PD. PASAR "BAUNTUNG BATUAH"'
            ],
            [
                'name' => 'BARAMARTA'
            ]
        ]);
    }
}
