<?php

use Illuminate\Database\Seeder;

class IndicatorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('indicators')->insert([
            // Disnakbun
            [
                'office_id' => 13,
                'unit_id' => 3,
                'name' => 'Populasi Ternak Sapi',
                'created_by' => 4
            ],
            [
                'office_id' => 13,
                'unit_id' => 3,
                'name' => 'Populasi Ternak Kerbau',
                'created_by' => 4
            ],
            [
                'office_id' => 13,
                'unit_id' => 3,
                'name' => 'Populasi Ternak Kambing, Domba',
                'created_by' => 4
            ],
            [
                'office_id' => 13,
                'unit_id' => 3,
                'name' => 'POpulasi Ternak Ayam Pedagang',
                'created_by' => 4
            ],
            // Dukcapil
            [
                'office_id' => 28,
                'unit_id' => 3,
                'name' => 'Kepuasan masyarakat dalam pelayanan',
                'created_by' => 4
            ],
            [
                'office_id' => 28,
                'unit_id' => 3,
                'name' => 'Cakupan kepemilikan KK',
                'created_by' => 4
            ]
        ]);
    }
}
