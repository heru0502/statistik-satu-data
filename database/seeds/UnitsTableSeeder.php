<?php

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            [
                'name' => 'Rupiah',
                'symbol' => 'Rp',
                'symbol_position' => 'left',
                'number_format' => 'currency',
                'created_by' => 1
            ],
            [
                'name' => 'Pcs',
                'symbol' => 'Pcs',
                'symbol_position' => 'right',
                'number_format' => 'integer',
                'created_by' => 1
            ],
            [
                'name' => 'Ekor',
                'symbol' => 'Ekor',
                'symbol_position' => 'right',
                'number_format' => 'integer',
                'created_by' => 1
            ],
            [
                'name' => 'Berat (Kg)',
                'symbol' => 'Kg',
                'symbol_position' => 'right',
                'number_format' => 'decimal',
                'created_by' => 1
            ],
            [
                'name' => 'Liter',
                'symbol' => 'liter',
                'symbol_position' => 'right',
                'number_format' => 'decimal',
                'created_by' => 1
            ]              
        ]);
    }
}
