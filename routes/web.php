<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->prefix('admin')->namespace('Admin')->group(function () {
    Route::get('/dashboard/{id?}', 'DashboardController@index')->name('dashboard.index');
    Route::get('/report', 'ReportController@index')->name('report.index');
    Route::post('/report', 'ReportController@report')->name('report.report');
    Route::resources([
        'office' => 'OfficeController',
        'employee' => 'EmployeeController',
        'unit' => 'UnitController',
        'indicator' => 'IndicatorController',
        // 'performance-indicator' => 'PerformanceIndicatorController'
    ]);

    Route::resource('performance-indicator', 'PerformanceIndicatorController')->except([
        'edit'
    ]);
    Route::get('/performance-indicator/{office_id}/{year}/edit', 'PerformanceIndicatorController@edit')->name('performance-indicator.edit');
});


Route::prefix('api')->namespace('Api')->group(function () {
    Route::get('/office', 'PerformanceIndicatorController@offices');
    Route::get('/indicator/{id}', 'PerformanceIndicatorController@indicators');
    Route::get('/years/{id}', 'PerformanceIndicatorController@years');
});